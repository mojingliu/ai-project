#!/bin/bash
line="$(grep -n "Piano right" "$1" | cut -f1 -d:)"
echo "start at: $line"
tail $1 -n +"$line" > tail.txt
endline="$(grep -n -m 1 "TrkEnd" tail.txt | cut -f1 -d:)"
echo "Track end: $endline"
head -n +"$endline" tail.txt > head.txt
cat head.txt | grep -i "n=" >edit.txt
cat edit.txt | awk '{print $1" "$4}' | sed 's/n=//g' > "b$1"
