% kernel mid
% version 2.2: abstract feature, train kernel, project to PCA
% Yulin April 6, 2015
 
clear all;
close
addpath('subfunction');
load trackYiruma.mat

track=track(:,1:2); % delete volume
time=track(:,1);
pitch=track(:,2);

plot(track(:,1),track(:,2),'.')
xlabel('time');ylabel(pitch);title('raw track')
saveas(gcf,'rawTrack.eps','psc2')

%% collect the right-hand track: pitch > 80
track1Ind=find(pitch>=80);
track1=track(track1Ind,:);
figure;plot(track1(:,1),track1(:,2),'.')


%% Yulin April 6, 2015. Successful feature!!! 
track5(:,2)=mapDisc(track1(:,2),[80 81 83 85 86 88],1:6);   % map to unit coordinates
track5(:,1)=track1(:,1)/2^5;    % 32 is the minimal interval
tEnd1=track5(end,1);            % 1300 is the last time period
trackMono=zeros(tEnd1,1);       
for i=1:length(track5(:,1))     % 517
    trackMono(track5(i,1))=track5(i,2);     % take the second choice [5 3] --> 3
end
for i=2:length(trackMono)
    if trackMono(i)==0
        trackMono(i)=trackMono(i-1);
    end
end
tEnd2=2^10;                     % 2^10 < total time interval 1300
trackMono1=trackMono(2^7+1:tEnd2+2^7);      % delete the beginning and ends
trackOcta=reshape(trackMono1,tEnd2/8,8);    % 2^7=128

%% --------------  build feature by every 8 digits !!!!!!!!!!!!!!!!!!!!!!
feature=zeros(tEnd2/8,8);
feature(1,:)=trackOcta(1,:);
for i=1:tEnd2/8
    feature(i,2:end)=diff(trackOcta(i,:));  % use diff for distance measure
end

close;figure;plot(feature(:,2),feature(:,3),'.')    % just for showing
%% --------------- train kernel, project to 3 pca
sigma=5;    % a good value. can be optimized by function evalclusters()
% -------------------- gauss kernel
[eigvec,K_center]= kernelpca_tutorial1(feature',sigma); 

% Projecting the data in lower dimensions
num_dim=2;
data_out = zeros(num_dim,size(feature,1));
for count = 1:num_dim
    data_out(count,:) = eigvec(:,count)'*K_center';
end
data_out=data_out';
data_out=normc(data_out);

%% --------------------- display change of melody
figure;
Interval2=8;
for id=1:16
    id
    hold off;
    plot(data_out(:,1),data_out(:,2),'.')
    hold on;
    plot(data_out((id-1)*Interval2+1:(id-1)*Interval2+Interval2,1),...
        data_out((id-1)*Interval2+1:(id-1)*Interval2+Interval2,2),'ko');
    legend('all',sprintf('melody%d',id))
    saveas(gcf,sprintf('melody%d.eps',id),'psc2')
    disp('press any key to continue')
    % pause
end


