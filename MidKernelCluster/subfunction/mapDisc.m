function y=mapDisc(x,xTable,yTable)
% no repeat in xTable

y=zeros(length(x),1);
for i=1:length(x)
    y(i)=yTable(find(xTable==x(i)));
end
