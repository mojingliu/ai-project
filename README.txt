==========================================================================
* By Yulin Shi and Mojing Liu
* AI ECSE 526
* MCGILL WINTER 2015
==========================================================================


This is the report source documents for AI ECSE 526 WINTER 2015.

ALL FILE CAN BE FOUND AT https://bitbucket.org/mojingliu/ai-project


1. jSymbolic directory
This is an extension to the jSymbolic project within the jMir project found at:
http://jmir.sourceforge.net/jSymbolic.html

The extension is fully for educational purpose. 

The extension and modification includes:
- Added midi.java within the jsymbolic.datatypes package as a midi object which has 
- Added input.java within the jsymbolic.gui package
- Modified saveFeatureDefinitions of MIDIFeatureProcessor to change the XML output to a arraylist of song which for each midi file input create a midi object and gives the appropriate feature values and definitions
- Modified extractFeatures of FeatureSelectorPanel to read from a database txt in order to speed up processing and start the display of the music recognition window.

**********To Run***********

In order to run the jSymbolic project, it is necessary to use netbeans and open the jSymbolic folder as project. It is currently NOT POSSIBLE TO BUILD the project due to data dependencies. However, netbeans is fully capable of running the project by clicking the arrow key. 

Steps:
1. Open jSymbolic project in netbeans
2. Run Project
3. The initial jSymbolic MIDI feature extractor window is shown, select add recording from mididatabase. Now it loads directly instead from database.txt which contains the precomputed values of 271 songs for 101 features. No selection is needed. 
4. After adding at least one recording, click on extract features button and the message Features successfully extracted and saved message box should appear as well as the music recognition window. Click OK on the DONE window. Now on the Music Recognition window, select Choose Sample to choose music sample. The test vector are located within testmidi. Screenshot at SC1.PNG
5. After selecting a sample input, click on process and the corresponding values will display.Screenshot at SC2.PNG
NOTE: There is also additional information displayed on the netbeans console such as distance and time. Screenshot at SC3.PNG

2. miditxt and textfile directory
This directory was used to translate midi to txt file for MATLAB processing

3. report directory
This directory was used to generate the report.pdf

4. MidKernelCluster directory

	1. "midKernelMain.m" is the main function. Press F5 to run it.
	2. use int value for express midi information.
	3. divide into 2^7 segements, each includes 2^3 pitches.
	4. use 2^7 instances, each in 6-dimensional feature space
	5. used gauss kernel
	6. projected to 2-D PCA 


PLEASE CONTACT FOR ANY ISSUES! THANKS!