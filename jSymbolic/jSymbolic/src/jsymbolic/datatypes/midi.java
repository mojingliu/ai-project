/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package jsymbolic.datatypes;

import java.io.File;
import java.util.ArrayList;
/*
* By Yulin Shi and Mojing Liu
* AI ECSE 526
* MCGILL WINTER 2015
*/
public class midi {
    
    final int MAX_NUM_FEATURE = 112;
    String author = "";
    String path = "";
    String name = "";
    int index;
    String[] features;
    double[] values;
    
    public midi (String path){
        this.path = path;
        File file = new File(path);
        this.author = file.getParentFile().getName();
        this.name =  file.getName();
        features = new String[MAX_NUM_FEATURE];
        values = new double[MAX_NUM_FEATURE];
        this.index =0;
    }
    
    public midi (String name, String author){
        this.path = "";
        this.author = author;
        this.name =  name;
        this.features = new String[MAX_NUM_FEATURE];
        values = new double[MAX_NUM_FEATURE];
        this.index =0;
    }
    
    
    
    
    public int getindex (){
        return this.index;
        
    }
    
    public void addfeature(String name, double value){
        
        this.features[this.index] = name;
        this.values[this.index] = value;
        this.index++;
        
    }
    
    public double[] getfeature(){
        return this.values;
        
    }
    
    public String get_author(){
        return this.author;
    }
    
    public String get_name(){
        return this.name;
    }
    
    
    
    public void tostring(){
        
        // System.out.println("Name: " + this.name);
        //System.out.println("Author: " + this.author);
        //System.out.println("Path: " + this.path);
        
        //        System.out.println("Features are:");
        System.out.println("Author:" + this.author+" Name: "+this.name+",");
        // for (int i = 0 ; i < MAX_NUM_FEATURE; i++){
        //         System.out.print(this.values[i]+",");
        // }
        // System.out.println("");
        
        
    }
}
